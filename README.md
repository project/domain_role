CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

Domain role module provides per-domain role functionality on Domain Access sites, for Drupal 8.
It leverages the domain_config submodule, providing a different configuration for user roles for each domain, and provides a UI to manage them.

This module overrides both the core User entity class, and the Cookie authentication provider, and thus may conflict with sites using alternative authentication providers and/or otherwise override the user entity.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/domain_role

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/domain_role

REQUIREMENTS
------------

This module requires the following modules:

 * Domain (https://www.drupal.org/project/domain)
 * Domain Configuration

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. This module partitions user roles so that each user account must be assigned a role on each domain they should have access to, instead of having the same roles across all domains.

MAINTAINERS
-----------

Current maintainers:
 * John Locke (freelock) - https://www.drupal.org/u/freelock
